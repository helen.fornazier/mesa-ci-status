import argparse
import concurrent.futures
import gitlab
import os

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')


def get_pipelines_counter_per_page(project, period, page):
    list_args = {
        "as_list": False,
        "per_page": 100,
        "page": page,
        "updated_after": period[0] + "T00:00:00.000Z",
        "updated_before": period[1] + "T23:59:59.999Z",
        "username": 'marge-bot',
        "scope": "finished",
        "source": "merge_request_event"
    }
    pipelines = project.pipelines.list(**list_args)
    n_failed = 0
    n_sucess = 0
    for pipeline in pipelines:
        if pipeline.status == "success":
            n_sucess += 1
        elif pipeline.status == "failed":
            n_failed += 1
        elif pipeline.status == "canceled":
            # ignore
            pass
        else:
            raise Exception("Unknown status ",  pipeline.status)

    return n_failed, n_sucess


def get_pipelines_counter_parallel_page_requests(project, period, init_page,
                                                 n_pages):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = executor.map(get_pipelines_counter_per_page,
                               [project] * n_pages,
                               [period] * n_pages,
                               range(init_page, init_page + n_pages))
    return list(results)


def print_status(status_obj):
    text = """
PERIOD: {from} - {until}
FAILED MERGE PIPELINES: {n_failed}/{n_total} - {failing_rate:.2f}%

INFORMATION:
    All numbers only considers mesa/mesa.
    FAILED MERGE PIPELINES: Calculated from finished pipelines triggered by Marge in a merge request that blocks the MR from being merged
""".format(**status_obj)  # noqa: E501
    print(text)


def get_pipelines_counter(project, period):
    results = []
    page = 1
    n_pages = 4
    while True:
        print("Processing pages " + str(page) + ".." + str(page + n_pages - 1))
        results_page = get_pipelines_counter_parallel_page_requests(project,
                                                                    period,
                                                                    page,
                                                                    n_pages)
        results += results_page
        if (0, 0) in results_page:
            break
        page += n_pages

    # concatenate lists and return
    return results


def main(period):
    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
    gl.auth()
    project = gl.projects.get(GITLAB_PROJECT_PATH)
    res = get_pipelines_counter(project, period)
    n_failed, n_sucess = (sum(i) for i in zip(*res))
    n_total = n_failed + n_sucess
    failing_rate = 100 * n_failed / n_total if n_total else 0

    status_obj = {
        "from": period[0],
        "until": period[1],
        "n_failed": n_failed,
        "n_total": n_total,
        "failing_rate": failing_rate,
    }
    print_status(status_obj)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-df", "--datefrom", help="Starting date XXXX-XX-XX",
                        required=True)
    parser.add_argument("-du", "--dateuntil", help="End date XXXX-XX-XX",
                        required=True)
    args = parser.parse_args()
    main((args.datefrom,  args.dateuntil))
